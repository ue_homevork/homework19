﻿
#include <iostream>
#include <string>

class Animal
{
public:
    virtual void Voice() = 0;
};

class Dog : public Animal
{
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "Murrr!\n";
    }
};

class Fox : public Animal
{
    void Voice() override
    {
        std::cout << "Frfrfr!\n";
    }
};


int main()
{
    //создаю массив
    const int size = 3;
    Animal* Animals[size] = { new Dog,new Cat, new Fox };
    for (int i = 0; i < size; i++)
    {
        Animals[i]->Voice();
    }
}
